﻿using System;
using System.Collections.Generic;
using System.Text;

namespace humusmazegenerator.Randomizer
{
    internal class MD5
    {
        private byte[] hashBytes;
        internal MD5(string input)
        {
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                hashBytes = md5.ComputeHash(inputBytes);
            }
        }

        internal int ToInt()
        => BitConverter.ToInt32(hashBytes, 0);

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hashBytes.Length; i++)
            {
                sb.Append(hashBytes[i].ToString("X2"));
            }
            return sb.ToString();
        }
    }
}

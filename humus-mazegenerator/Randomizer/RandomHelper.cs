﻿using System;

namespace humusmazegenerator.Randomizer
{
    class RandomHelper
    {
        private static Random random;
        internal int GetRandomNumber(int Max) => random.Next(Max);
        internal bool GetRandomBool() => random.Next() % 2 == 0;

        public RandomHelper(string seed)
        {
            var md5 = new MD5(seed);
            random = new Random(md5.ToInt());
        }

    }
}

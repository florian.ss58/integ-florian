﻿using humusmazegenerator.Cells;

namespace humus_mazegenerator.Rooms
{
    internal class RoomCreator
    {
        internal static Cell[][] Create(Exits Dir)
        => (Dir.NumberOfExit == 0) ? new FullRoom().Cells : CreateDefaultRoom(Dir);

        private static Cell[][] CreateDefaultRoom(Exits Dir)
        {
            var room = new DefaultRoom().Cells;
            if (Dir.North)
                room = RoomHelper.AddNorthDoor(room);
            if (Dir.East)
                room = RoomHelper.AddEastDoor(room);
            if (Dir.South)
                room = RoomHelper.AddSouthDoor(room);
            if (Dir.West)
                room = RoomHelper.AddWestDoor(room);
            return room;
        }
    }
}


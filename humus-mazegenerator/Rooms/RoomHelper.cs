﻿using humusmazegenerator;
using humusmazegenerator.Cells;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace humus_mazegenerator.Rooms
{
    struct Exits
    {
        internal bool North;
        internal bool East;
        internal bool South;
        internal bool West;
        internal int NumberOfExit;
    }

    internal class RoomHelper
    {
        internal const int RoomSize = 8;

        internal static Cell[] CellsFullRow(CellType type)
        {
            var r = new Cell[RoomSize]
            {
                    new Cell(){ Type=type },
                    new Cell(){ Type=type },
                    new Cell(){ Type=type },
                    new Cell(){ Type=type },
                    new Cell(){ Type=type },
                    new Cell(){ Type=type },
                    new Cell(){ Type=type },
                    new Cell(){ Type=type }
                };
            return r;
        }

        internal static Cell[][] AddNorthDoor(Cell[][] c)
        {
            c[0][(RoomSize / 2) - 1] = new Cell { Type = CellType.Door };
            c[0][RoomSize / 2] = new Cell { Type = CellType.Door };
            return c;
        }

        internal static Cell[][] AddEastDoor(Cell[][] c)
        {
            c[(RoomSize / 2) - 1][RoomSize - 1] = new Cell { Type = CellType.Door };
            c[RoomSize / 2][RoomSize - 1] = new Cell { Type = CellType.Door };
            return c;
        }

        internal static Cell[][] AddSouthDoor(Cell[][] c)
        {
            c[RoomSize - 1][(RoomSize / 2) - 1] = new Cell { Type = CellType.Door };
            c[RoomSize - 1][RoomSize / 2] = new Cell { Type = CellType.Door };
            return c;
        }

        internal static Cell[][] AddWestDoor(Cell[][] c)
        {
            c[(RoomSize / 2) - 1][0] = new Cell { Type = CellType.Door };
            c[RoomSize / 2][0] = new Cell { Type = CellType.Door };
            return c;
        }

        internal static Exits[][] DeleteFullRowsAndColumns(Exits[][] source)
        { 
            var closedroom = new Exits { North = false, East = false, South = false, West = false, NumberOfExit = 0 };
            return source.DeleteFullRowOf(closedroom)
                         .Rotate90()
                         .DeleteFullRowOf(closedroom)
                         .Rotate90(false);
        }
    }
}

﻿using humusmazegenerator;
using humusmazegenerator.Cells;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace humus_mazegenerator.Rooms
{
    internal class FullRoom
    {
        internal Cell[][] Cells
        {
            get
            {
                return
                Enumerable.Range(0, RoomHelper.RoomSize)
                          .ToList()
                          .Select(i => RoomHelper.CellsFullRow(CellType.Wall))
                          .ToArray();
            }
        }
    }
}

﻿using humusmazegenerator;
using System;
using System.Collections.Generic;
using System.Text;

namespace humus_mazegenerator.Rooms
{
    internal class RoomConnection
    {
        internal Exits[][] Rooms;

        public RoomConnection(Maze maze)
        {
            var mazewidth = maze.InnerMap.Width;
            var mazeheight = maze.InnerMap.Height;
            Rooms = new Exits[mazeheight][];
            for (int i = 0; i < mazeheight; i++)
            {
                Rooms[i] = new Exits[mazewidth];
                for (int j = 0; j < mazewidth; j++)
                {
                    var North = false;
                    var East = false;
                    var South = false;
                    var West = false;
                    var NumberOfExit = 0;
                    if (maze.InnerMap[i, j])
                    {
                        // North
                        if (maze.InnerMap[i - 1, j])
                        {
                            NumberOfExit++;
                            North = true;
                        }
                        // East
                        if (maze.InnerMap[i, j + 1])
                        {
                            NumberOfExit++;
                            East = true;
                        }
                        // South
                        if (maze.InnerMap[i + 1, j])
                        {
                            NumberOfExit++;
                            South = true;
                        }
                        // West
                        if (maze.InnerMap[i, j - 1])
                        {
                            NumberOfExit++;
                            West = true;
                        }
                    }
                    Rooms[i][j] = new Exits { North = North, East = East, South = South, West = West, NumberOfExit = NumberOfExit };
                }
            }
        }
    }
}

﻿using humusmazegenerator;
using humusmazegenerator.Cells;
using System;
using System.Collections.Generic;
using System.Text;

namespace humus_mazegenerator.Rooms

{
    internal class DefaultRoom
    {
        internal Cell[][] Cells
        {
            get
            {
                var c = new Cell[RoomHelper.RoomSize][];
                for (int i = 0; i < RoomHelper.RoomSize; i++)
                {
                    c[i] = new Cell[RoomHelper.RoomSize];
                    var row = (i == 0 || i == RoomHelper.RoomSize - 1) ? RoomHelper.CellsFullRow(CellType.Wall)
                                                                    : RoomHelper.CellsFullRow(CellType.Empty);
                    for (int j = 0; j < RoomHelper.RoomSize; j++)
                    {
                        c[i][j] = (j == 0 || j == RoomHelper.RoomSize - 1) ? new Cell { Type = CellType.Wall } : row[j];
                    }
                }

                return c;
            }
        }
    }
}

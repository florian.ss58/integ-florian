﻿namespace humusmazegenerator
{
    /// <summary>
    /// Contains a position.
    /// Note: Struct really is faster then class
    /// </summary>
    internal struct MazePoint
    {
        internal int X, Y;

        internal MazePoint(int X, int Y)
        {
            this.X = X;
            this.Y = Y;
        }

        public override string ToString()
        {
            return "MazePoint, X: " + X + ", Y: " + Y;
        }
    }
}

﻿using System;
using System.Runtime.CompilerServices;

namespace humusmazegenerator
{
    internal class QuatroStack
    {
        private int cur = -1;
        public QuatroList InnerList;

        internal QuatroStack()
        {
            InnerList = new QuatroList();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal void Push(int input)
        {
            cur++;
            InnerList[cur] = input;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal int Peek()
        {
            if (cur == -1)
                throw new ArgumentException("Stack is empty");
            return InnerList[cur];
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal int Pop()
        {
            if (cur == -1)
                throw new ArgumentException("Stack is empty");
            cur--;
            return InnerList[cur + 1];
        }

        internal int Count
        {
            get { return cur + 1; }
        }
    }
}

﻿using humusmazegenerator.InnerMaps;
using humusmazegenerator.Randomizer;
using System;


namespace humusmazegenerator
{
    internal class Maze
    {
        private const int SizeRate = 4;

        internal int Width
        {
            get;
        }

        internal int Height
        {
            get;
        }

        internal InnerMap InnerMap { get; }

        private RandomHelper random;

        internal Maze(int RoomsSurface, RandomHelper Random)
        {
            random = Random;
            (Width, Height) = GetMazeSize(RoomsSurface);
            InnerMap = new BoolInnerMap(Width, Height);
        }

        private (int width, int height) GetMazeSize(int TotalRoomSize)
        {
            var totalSize = (TotalRoomSize * SizeRate);
            var side = (int)Math.Sqrt(totalSize);
            var gap = (int)Math.Round((double)((totalSize - (side * side)) / side));

            return (width: random.GetRandomBool() ? side : side + gap,
                    height: random.GetRandomBool() ? side : side + gap);
        }
    }
}


﻿using humus_mazegenerator.Rooms;
using humusmazegenerator.Cells;
using humusmazegenerator.Generators;
using System.Linq;


namespace humusmazegenerator
{
    public class Generate
    {
        public static Cell[][] GenerateMaze(string mazename)
        {
            var Rnd = new Randomizer.RandomHelper(mazename);
            // get maze structure by generating a simple maze
            var maze = new AlgorithmBacktrackSmartMemory().Generate(16, Rnd, null);//64 for prod, less for debug

            // detect room connection
            var rooms = new RoomConnection(maze).Rooms;

            // delete full rows and columns
            var cleanrooms = RoomHelper.DeleteFullRowsAndColumns(rooms);

            // create final maze
            var finalmaze = Enumerable.Range(0, cleanrooms.Length * 8)
                .Select(i => new Cell[cleanrooms[0].Length * 8])
                .ToArray();

            // place cell by preshape room
            for (int i = 0; i < cleanrooms.Length; i++)
            {
                for (int j = 0; j < cleanrooms[0].Length; j++)
                {
                    var room = RoomCreator.Create(cleanrooms[i][j]);
                    for (int k = 0; k < room.Length; k++)
                    {
                        for (int l = 0; l < room[0].Length; l++)
                        {
                            finalmaze[k + i * 8][l + j * 8] = room[k][l];
                        }
                    }
                }
            }
            /// different kind of doors : simple door, locked door, trapped door, tresured door
            //maze.InnerMap.ToCells().MazeToString().ToConsole();
            return finalmaze;//maze.InnerMap.ToCells();
        }
    }
}

﻿using humusmazegenerator.Cells;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;

namespace humusmazegenerator
{
    internal static class MazeExtensionMethods
    {
        //internal static Cell[,] FillWith(this Cell[,] Maze, IEnumerable<Rectangle> Rectangles, Func<CellType, bool> ConditionToApplyType, CellType Type)
        //{
        //    Enumerable.Range(0, Maze.GetUpperBound(0))
        //              .ToList()
        //              .ForEach(i =>
        //              Enumerable.Range(0, Maze.GetUpperBound(0))
        //                        .ToList()
        //                        .ForEach(j =>
        //                    Rectangles.Where(r => r.IntersectsWith(new Rectangle(i, j, 1, 1)) && ConditionToApplyType(Maze[i, j].Type))
        //                              .ToList()
        //                              .ForEach(r => Maze[i, j].Type = Type)));
        //    return Maze;
        //}

        //static internal Cell[,] AddDoors(this Cell[,] Maze, IEnumerable<Rectangle> Rooms)
        //    => Maze.FillWith(Door.GetDoors(Maze, Rooms), (t) => t == CellType.Empty, CellType.Door);

        //static internal Cell[,] CloseWideSpace(this Cell[,] Maze, IEnumerable<Rectangle> Rooms)
        //    => Maze.FillWith(Door.CloseWideDoors(Maze), (t) => t == CellType.Empty, CellType.Wall);


        //private static Random random = new Random();

        /// <summary>
        /// Method to randomly sort a list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sequence"></param>
        /// <returns></returns>
        //internal static List<T> RandomPermutation<T>(this List<T> sequence)
        //{
        //    T[] retArray = sequence.ToArray();

        //    for (int i = 0; i < retArray.Length - 1; i += 1)
        //    {
        //        int swapIndex = random.Next(i + 1, retArray.Length);
        //        T temp = retArray[i];
        //        retArray[i] = retArray[swapIndex];
        //        retArray[swapIndex] = temp;
        //    }

        //    return retArray.ToList();
        //}

        /// <summary>
        /// Method to randomly sort a list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sequence"></param>
        /// <returns></returns>
        //internal static List<T> RandomPermutation<T>(this List<T> sequence, int seed)
        //{
        //    Random random = new Random(seed);
        //    T[] retArray = sequence.ToArray();

        //    for (int i = 0; i < retArray.Length - 1; i += 1)
        //    {
        //        int swapIndex = random.Next(i + 1, retArray.Length);
        //        T temp = retArray[i];
        //        retArray[i] = retArray[swapIndex];
        //        retArray[swapIndex] = temp;
        //    }

        //    return retArray.ToList();
        //}

        //internal static T[][] ToJaggedArray<T>(this T[,] twoDimensionalArray)
        //{
        //    int rowsFirstIndex = twoDimensionalArray.GetLowerBound(0);
        //    int rowsLastIndex = twoDimensionalArray.GetUpperBound(0);
        //    int numberOfRows = rowsLastIndex + 1;

        //    int columnsFirstIndex = twoDimensionalArray.GetLowerBound(1);
        //    int columnsLastIndex = twoDimensionalArray.GetUpperBound(1);
        //    int numberOfColumns = columnsLastIndex + 1;

        //    T[][] jaggedArray = new T[numberOfRows][];
        //    for (int i = rowsFirstIndex; i <= rowsLastIndex; i++)
        //    {
        //        jaggedArray[i] = new T[numberOfColumns];

        //        for (int j = columnsFirstIndex; j <= columnsLastIndex; j++)
        //        {
        //            jaggedArray[i][j] = twoDimensionalArray[i, j];
        //        }
        //    }
        //    return jaggedArray;
        //}

        internal static T[][] Rotate90<T>(this T[][] source, bool clockwize = true)
        => clockwize ? source.Rotated90Clockwize() : source.Rotated90Anticlockwize();

        internal static T[][] Rotated90Clockwize<T>(this T[][] source)
        => Enumerable.Range(0, source[0].Length)
                     .ToList()
                     .Select(i => source.Select(p => p[i]).Reverse().ToArray())
                     .ToArray();

        internal static T[][] Rotated90Anticlockwize<T>(this T[][] source)
        => Enumerable.Range(0, source[0].Length)
                     .ToList()
                     .Select(i => source.Select(p => p[i]).ToArray())
                     .Reverse()
                     .ToArray();

        internal static T[][] DeleteFullRowOf<T>(this T[][] source, T item)
        => source.DeleteRowLike<T>(Enumerable.Range(0, source[0].Length)
                                             .ToList()
                                             .Select(i => item)
                                             .ToArray());

        internal static T[][] DeleteRowLike<T>(this T[][] source, T[] pattern)
        => source.Where(r => !r.SequenceEqual(pattern)).ToArray();

        internal static void ToConsole(this string input) => Console.WriteLine(input);

        internal static void ToFile(this string input)
        {
            var file = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), DateTime.Now.ToString("yyyyMMddHHmmss") + ".maze");
            File.AppendAllText(file, input);
            Console.WriteLine("file generated : " + file);
        }

        internal static string MazeToString(this Cell[][] Dungeon)
        {
            var str = string.Empty;

            for (var i = 0; i <= Dungeon.Length; i++)
            {
                str += Environment.NewLine;
                for (var j = 0; j <= Dungeon[0].Length; j++)
                {
                    if (Dungeon[i][j] == null)
                        str += "|N";
                    else
                        switch (Dungeon[i][j].Type)
                        {
                            case CellType.Wall:
                                str += "|X";
                                break;
                            case CellType.Door:
                                str += "|D";
                                break;
                            case CellType.Empty:
                                str += "| ";
                                break;
                        }
                }
            }
            return str;
        }
    }
}

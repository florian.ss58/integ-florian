﻿using humusmazegenerator.Cells;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace humusmazegenerator.InnerMaps
{
    internal abstract class InnerMap
    {
        internal int Width { [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get; }

        internal int Height { [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get; }

        internal InnerMap(int width, int height)
        {
            Width = width;
            Height = height;
        }

        /// Info about mazes:
        /// 0 = False = Wall = Black
        /// 1 = True = Empty = White
        internal Cell[][] ToCells()
        {
            var cells = new Cell[Width][];
            for (int y = 0; y < this.Height; y++)
            {
                cells[y] = new Cell[Height];

                for (int x = 0; x < this.Width; x++)
                {
                    cells[x][y] = this[x, y] ? new Cell{ Type = CellType.Empty } : new Cell{ Type = CellType.Wall };
                }
            }
            return cells;
        }

        //virtual can be overidden
        internal virtual void Print()
        {
            StringBuilder build = new StringBuilder();
            for (int y = 0; y < this.Height; y++)
            {
                for (int x = 0; x < this.Width; x++)
                {
                    bool b = this[x, y];
                    if (b)
                    {
                        build.Append('1');
                    }
                    else
                    {
                        build.Append('0');
                    }
                }
                build.AppendLine();
            }
            Console.WriteLine(build);
        }

        /// <summary>
        /// Info about mazes:
        /// 0 = False = Wall = Black
        /// 1 = True = Empty = White
        /// </summary>

        //abstract must be overidden
        public abstract bool this[int x, int y] { get; set; }
    }
}

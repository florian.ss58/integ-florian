﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace humusmazegenerator.InnerMaps
{
    internal class BoolInnerMap : InnerMap
    {
        private readonly bool[,] innerData;

        public BoolInnerMap(int width, int height)
            : base(width, height)
        {
            innerData = new bool[width, height];
        }

        public BoolInnerMap(int width, int height, bool[,] data)
            : base(width, height)
        {
            innerData = data;
        }

        public override bool this[int x, int y]
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get
            {
                return innerData[x, y];
            }
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            set
            {
                innerData[x, y] = value;
            }
        }
    }
}

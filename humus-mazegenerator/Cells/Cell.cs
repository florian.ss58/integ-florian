﻿namespace humusmazegenerator.Cells
{
    public class Cell
    {
        public CellType Type = CellType.Wall;

        public override string ToString()
        {
            return Type.ToString(); 
        }
    }
}

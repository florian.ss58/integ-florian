﻿using humusmazegenerator.Cells;
using Microsoft.AspNetCore.Mvc;
using System;

namespace HmgAPI.Controllers
{
    [Route("api/maze")]
    [ApiController]
    public class MazeController : ControllerBase
    {

        // GET maze
        [HttpGet]
        public ActionResult<Cell[][]> Generate()
        => GenerateMaze(DateTime.Now.ToString("o"));

        // GET maze/test
        [HttpGet("{Name}", Name = "")]
        public ActionResult<Cell[][]> Generate(string Name)
        => GenerateMaze(Name);


        private Cell[][] GenerateMaze(string Name)
        {
            try
            {
                return humusmazegenerator.Generate.GenerateMaze(Name);
            }
            catch
            {
                return new Cell[0][] { };
            }
        }

    }
}
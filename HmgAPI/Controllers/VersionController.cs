﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace HmgAPI.Controllers
{
    [Route("api/version")]
    [ApiController]
    public class VersionController : ControllerBase
    {
        // GET version
        [HttpGet]
        public ActionResult<string> Version()
            => typeof(VersionController).Assembly.GetName().Version.ToString();

    }
}

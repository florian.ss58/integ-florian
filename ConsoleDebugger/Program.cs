﻿using humusmazegenerator;
using humusmazegenerator.Cells;
using System;
using System.IO;
using System.Reflection;

namespace ConsoleDebugger
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length==1 && !string.IsNullOrWhiteSpace(args[0]) && args[0].Length < 32)
            {
                ToConsole(MazeToString(Generate.GenerateMaze(args[0])));
                return;
            }

            // interractive mode
            string input;
            do
            {
                Console.WriteLine("");
                Console.WriteLine("Press Enter to generate a maze");
                input = Console.ReadLine();
                var m = MazeToString(Generate.GenerateMaze(input));
                //ToFile(m);
                ToConsole(m);
                //Console.WriteLine("Number of rooms :");
                //if (int.TryParse(input, out int NbRooms))
                //{
                //    var m = MazeToString(Generate.GenerateMazev1(NbRooms));
                //    ToConsole(m);
                //    ToFile(m);
                //}
            } while (input != "q");
        }

        static void ToConsole(string input)
        {
            Console.WriteLine(input);
        }

        static void ToFile(string input)
        {
            var file = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), DateTime.Now.ToString("yyyyMMddHHmmss") + ".maze");
            File.AppendAllText(file, input);
            Console.WriteLine("file generated : " + file);
        }

        static string MazeToString(Cell[][] Dungeon)
        {
            var str = string.Empty;

            for (var i = 0; i < Dungeon.Length; i++)
            {
                str += Environment.NewLine;
                for (var j = 0; j < Dungeon[0].Length; j++)
                {
                    if (Dungeon[i][j] == null)
                        str += "NN";
                    else
                        switch (Dungeon[i][j].Type)
                        {
                            case CellType.Wall:
                                str += "|X";
                                break;
                            case CellType.Door:
                                str += "|D";
                                break;
                            case CellType.Empty:
                                str += "| ";
                                break;
                        }
                }
            }
            return str;
        }
    }
}

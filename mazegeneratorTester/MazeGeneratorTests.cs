using Microsoft.VisualStudio.TestTools.UnitTesting;
using humusmazegenerator;
using System.Linq;

namespace mazegeneratorTester
{
    [TestClass]
    public class MazeGeneratorTests
    {
        private const string key0 = "FirstMaze";
        private const string key1 = "AnotherMaze";

        [TestMethod]
        public void SimpleMazeGeneration()
        {
            var m = Generate.GenerateMaze(key0);
            Assert.IsTrue(m.Length > 0);
        }

        [TestMethod]
        public void TwoIdenticalMazeGeneration()
        {
            var m0 = Generate.GenerateMaze(key0);
            var m1 = Generate.GenerateMaze(key0);
            Assert.IsTrue(m0.SelectMany(T => T).Select(T => T.Type).SequenceEqual(m1.SelectMany(T => T).Select(T => T.Type)));
        }

        [TestMethod]
        public void TwodifferentMazeGeneration()
        {
            var m0 = Generate.GenerateMaze(key0);
            var m1 = Generate.GenerateMaze(key1);
            Assert.IsTrue(!m0.SelectMany(T => T).Select(T => T.Type).SequenceEqual(m1.SelectMany(T => T).Select(T => T.Type)));
        }
    }
}
